<?php
class StudentInfo{

    public $std_id="Hello";
    public $std_name;
    public $std_cgpa;

    public  function set_std_id($std_id){
        $this->std_id=$std_id;
    }

    public  function get_std_id(){
        return $this->std_id;
    }

    public function set_std_name($std_name){
        $this->std_name=$std_name;
    }
    public function get_std_name(){
        return $this->std_name;
    }

    public function set_std_cgpa($std_cgpa){
        $this->std_cgpa=$std_cgpa;
    }
    public function get_std_cgpa(){
        return $this->std_cgpa;
    }

}

$obj = new StudentInfo;

//echo $obj->std_id;
$obj->set_std_name("Sharmin");
echo $obj->std_name;
$obj->set_std_id("139505");
echo $obj->std_id;
$obj->set_std_cgpa("4.0");
echo $obj->std_cgpa;

/*$obj->get_std_id("Qurashi");
echo $obj->std_id;
$obj->get_std_id("SEIP139505");
echo $obj->std_id;
$obj->get_std_id("5.0");
echo $obj->std_id;*/

$result = $obj->get_std_id();
echo $result;
$result = $obj->get_std_name();
echo $result;
$result = $obj->get_std_cgpa();
echo $result;

